# Copyright 2021 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG DOCKER_ENTRYPOINT_VERSION=792a7cc125d5b0b37e634fca762db92c5aca07d3

ARG MARIADB_IMAGE=mariadb
ARG MARIADB_VERSION=10.6.0

ARG DOWNLOADER_IMAGE=alpine
ARG DOWNLOADER_VERSION=3.13.5

FROM $DOWNLOADER_IMAGE:$DOWNLOADER_VERSION AS downloader

RUN \
    wget \
        --quiet \
        --output-document=/usr/local/bin/docker-entrypoint.sh \
        "https://raw.githubusercontent.com/MariaDB/mariadb-docker/${DOCKER_ENTRYPOINT_VERSION:-master}/docker-entrypoint.sh"

FROM $MARIADB_IMAGE:$MARIADB_VERSION

COPY --from=downloader /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
COPY mysqld-entrypoint /usr/local/bin/mysqld-entrypoint

RUN \
    chmod a+x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["mysqld-entrypoint"]

CMD ["mysqld"]
